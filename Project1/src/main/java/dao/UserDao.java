package dao;

import model.User;

import java.util.List;

public interface UserDao {
    User addUser(User user);
    User getUser(String name);
    List<User> getAllUsers();
}
