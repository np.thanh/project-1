package dao;

import model.Reimbursement;

import java.util.List;

public interface ReimbursementDao {
    List<Reimbursement> employeeViewAllTickets(Integer employeeId);
    void addReimbursement(Reimbursement reimbursement);
    List<Reimbursement> viewAllReimbursement();
    void updateStatus(Integer reimbId, Integer statusId, Integer resolverId);
    List<Reimbursement> viewAllReimbursementByStatus(Integer statusId);
}
