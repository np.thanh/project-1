package dao;

public class ConnectionUtil {
    public static String url = "jdbc:postgresql://" + System.getenv("POSTGRES_URI") + "/project1_db";
    public static String username = System.getenv("POSTGRES_USERNAME");
    public static String password = System.getenv("POSTGRES_PASSWORD");
}
