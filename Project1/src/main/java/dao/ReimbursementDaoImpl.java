package dao;

import model.Reimbursement;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementDaoImpl implements ReimbursementDao{
    private static ReimbursementDao reimbursementDao;

    private ReimbursementDaoImpl(){
        try{
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static ReimbursementDao getInstance(){
        if(reimbursementDao == null)
            reimbursementDao = new ReimbursementDaoImpl();
        return reimbursementDao;
    }



    @Override
    public List<Reimbursement> employeeViewAllTickets(Integer employeeId) {
        List<Reimbursement> reimbursements = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){

            String sql = "select * from ers_reimbursement where reimb_author = ?;";

            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setInt(1, employeeId);

            ResultSet rs = ps.executeQuery();


            while(rs.next()) {
                reimbursements.add(
                        new Reimbursement(rs.getInt(1),rs.getDouble(2),rs.getTimestamp(3),rs.getTimestamp(4)
                        , rs.getString(5), rs.getBytes(6), rs.getInt(7),
                                rs.getInt(8), rs.getInt(9), rs.getInt(10))
                );
            }

        }catch(SQLException e) {
            e.printStackTrace();
        }

        return reimbursements;
    }

    @Override
    public void addReimbursement(Reimbursement reimbursement) {
        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){
            String sql = "insert into ers_reimbursement values" +
                    "(default, ?, default, default, ?, ?, ?, null, ?, ?);";

            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setDouble(1, reimbursement.getReimbursementAmount());
            ps.setString(2, reimbursement.getReimbursementDescription());
            ps.setBytes(3, reimbursement.getReimbursementReceipt());
            ps.setInt(4, reimbursement.getReimbursementAuthor());
            ps.setInt(5, reimbursement.getReimbursementStatusID_FK());
            ps.setInt(6, reimbursement.getReimbursementTypeID_FK());

            ps.executeUpdate();


        }catch(SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Reimbursement> viewAllReimbursement() {
        List<Reimbursement> reimbursements = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){

            String sql = "select * from ers_reimbursement;";

            PreparedStatement ps = conn.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();


            while(rs.next()) {
                reimbursements.add(
                        new Reimbursement(rs.getInt(1),rs.getDouble(2),rs.getTimestamp(3),rs.getTimestamp(4)
                                , rs.getString(5), rs.getBytes(6), rs.getInt(7),
                                rs.getInt(8), rs.getInt(9), rs.getInt(10))
                );
            }

        }catch(SQLException e) {
            e.printStackTrace();
        }

        return reimbursements;
    }

    @Override
    public void updateStatus(Integer reimbId, Integer statusId, Integer resolverId) {
        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){
            String sql = "update ers_reimbursement set reimb_status_id_fk = ?, reimb_resolved = current_timestamp, reimb_resolver = ? where reimb_id = ?;";

            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setInt(1, statusId);
            ps.setInt(2, resolverId);
            ps.setInt(3, reimbId);

            ps.executeUpdate();


        }catch(SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Reimbursement> viewAllReimbursementByStatus(Integer statusId) {
        List<Reimbursement> reimbursements = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){

            String sql = "select * from ers_reimbursement where reimb_status_id_fk = ?;";

            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, statusId);

            ResultSet rs = ps.executeQuery();


            while(rs.next()) {
                reimbursements.add(
                        new Reimbursement(rs.getInt(1),rs.getDouble(2),rs.getTimestamp(3),rs.getTimestamp(4)
                                , rs.getString(5), rs.getBytes(6), rs.getInt(7),
                                rs.getInt(8), rs.getInt(9), rs.getInt(10))
                );
            }

        }catch(SQLException e) {
            e.printStackTrace();
        }

        return reimbursements;
    }
}
