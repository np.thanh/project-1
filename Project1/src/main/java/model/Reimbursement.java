package model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;
import java.util.Arrays;

public class Reimbursement {
    private Integer reimbursementID;
    private Double reimbursementAmount;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd @HH:mm:ss", timezone="EST")
    private Timestamp reimbursementSubmitted;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd @HH:mm:ss", timezone="EST")
    private Timestamp reimbursementResolved;
    private String reimbursementDescription;
    private byte[] reimbursementReceipt;
    private Integer reimbursementAuthor;
    private Integer reimbursementResolver;
    private Integer reimbursementStatusID_FK;
    private Integer reimbursementTypeID_FK;

    public Reimbursement() {
    }

    public Reimbursement(Integer reimbursementID, Integer reimbursementStatusID_FK) {
        this.reimbursementID = reimbursementID;
        this.reimbursementStatusID_FK = reimbursementStatusID_FK;
    }

    public Reimbursement(Integer reimbursementID, Double reimbursementAmount, Timestamp reimbursementSubmitted,
                         Timestamp reimbursementResolved, String reimbursementDescription, byte[] reimbursementReceipt,
                         Integer reimbursementAuthor, Integer reimbursementResolver, Integer reimbursementStatusID_FK,
                         Integer reimbursementTypeID_FK) {
        this.reimbursementID = reimbursementID;
        this.reimbursementAmount = reimbursementAmount;
        this.reimbursementSubmitted = reimbursementSubmitted;
        this.reimbursementResolved = reimbursementResolved;
        this.reimbursementDescription = reimbursementDescription;
        this.reimbursementReceipt = reimbursementReceipt;
        this.reimbursementAuthor = reimbursementAuthor;
        this.reimbursementResolver = reimbursementResolver;
        this.reimbursementStatusID_FK = reimbursementStatusID_FK;
        this.reimbursementTypeID_FK = reimbursementTypeID_FK;
    }

    public Integer getReimbursementID() {
        return reimbursementID;
    }

    public void setReimbursementID(Integer reimbursementID) {
        this.reimbursementID = reimbursementID;
    }

    public Double getReimbursementAmount() {
        return reimbursementAmount;
    }

    public void setReimbursementAmount(Double reimbursementAmount) {
        this.reimbursementAmount = reimbursementAmount;
    }

    public Timestamp getReimbursementSubmitted() {
        return reimbursementSubmitted;
    }

    public void setReimbursementSubmitted(Timestamp reimbursementSubmitted) {
        this.reimbursementSubmitted = reimbursementSubmitted;
    }

    public Timestamp getReimbursementResolved() {
        return reimbursementResolved;
    }

    public void setReimbursementResolved(Timestamp reimbursementResolved) {
        this.reimbursementResolved = reimbursementResolved;
    }

    public String getReimbursementDescription() {
        return reimbursementDescription;
    }

    public void setReimbursementDescription(String reimbursementDescription) {
        this.reimbursementDescription = reimbursementDescription;
    }

    public byte[] getReimbursementReceipt() {
        return reimbursementReceipt;
    }

    public void setReimbursementReceipt(byte[] reimbursementReceipt) {
        this.reimbursementReceipt = reimbursementReceipt;
    }

    public Integer getReimbursementAuthor() {
        return reimbursementAuthor;
    }

    public void setReimbursementAuthor(Integer reimbursementAuthor) {
        this.reimbursementAuthor = reimbursementAuthor;
    }

    public Integer getReimbursementResolver() {
        return reimbursementResolver;
    }

    public void setReimbursementResolver(Integer reimbursementResolver) {
        this.reimbursementResolver = reimbursementResolver;
    }

    public Integer getReimbursementStatusID_FK() {
        return reimbursementStatusID_FK;
    }

    public void setReimbursementStatusID_FK(Integer reimbursementStatusID_FK) {
        this.reimbursementStatusID_FK = reimbursementStatusID_FK;
    }

    public Integer getReimbursementTypeID_FK() {
        return reimbursementTypeID_FK;
    }

    public void setReimbursementTypeID_FK(Integer reimbursementTypeID_FK) {
        this.reimbursementTypeID_FK = reimbursementTypeID_FK;
    }

    @Override
    public String toString() {
        return "Reimbursement{" +
                "reimbursementID=" + reimbursementID +
                ", reimbursementAmount=" + reimbursementAmount +
                ", reimbursementSubmitted=" + reimbursementSubmitted +
                ", reimbursementResolved=" + reimbursementResolved +
                ", reimbursementDescription='" + reimbursementDescription + '\'' +
                ", reimbursementReceipt=" + Arrays.toString(reimbursementReceipt) +
                ", reimbursementAuthor=" + reimbursementAuthor +
                ", reimbursementResolver=" + reimbursementResolver +
                ", reimbursementStatusID_FK=" + reimbursementStatusID_FK +
                ", reimbursementTypeID_FK=" + reimbursementTypeID_FK +
                '}';
    }
}
