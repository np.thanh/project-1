package service;

import dao.UserDao;
import dao.UserDaoImpl;
import model.User;

import java.util.ArrayList;
import java.util.List;

public class  UserService {
    UserDao userDao;

    public UserService(){
        userDao = UserDaoImpl.getInstance();
    }

    public User addUser(User user){
        User checkingUser = userDao.getUser(user.getUsername());
        if (checkingUser != null)
            return null;
        else
            userDao.addUser(user);
        return userDao.getUser(user.getUsername());


    }

    public User getUser(User user){
        User checkingUser = userDao.getUser(user.getUsername());
        if (checkingUser == null)
            return null;
        if (!checkingUser.getPassword().equals(user.getPassword()))
            return null;
        return checkingUser;

    }

    public List<User> getAllUser(){
        return userDao.getAllUsers();
    }
}
