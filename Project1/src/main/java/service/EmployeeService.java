package service;

import dao.ReimbursementDao;
import dao.ReimbursementDaoImpl;
import model.Reimbursement;

import java.util.List;

public class EmployeeService {
    ReimbursementDao reimbursementDao;

    public EmployeeService(){
        reimbursementDao = ReimbursementDaoImpl.getInstance();
    }

    public List<Reimbursement> employeeViewReimbursement(Integer employeeID){
        return reimbursementDao.employeeViewAllTickets(employeeID);
    }

    public void addReimbursementRequest(Reimbursement reimbursement){
        reimbursementDao.addReimbursement(reimbursement);
    }
}
