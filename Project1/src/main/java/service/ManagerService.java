package service;

import dao.ReimbursementDao;
import dao.ReimbursementDaoImpl;
import model.Reimbursement;

import java.util.List;

public class ManagerService {
    ReimbursementDao reimbursementDao;

    public ManagerService(){
        reimbursementDao = ReimbursementDaoImpl.getInstance();
    }

    public List<Reimbursement> managerViewAllReimbursement(){
        return reimbursementDao.viewAllReimbursement();
    }
    public void managerUpdateStatus(Integer reimbId, Integer statusId, Integer resolverId){
        reimbursementDao.updateStatus(reimbId, statusId, resolverId);
    }

    public List<Reimbursement> managerViewAllRequestByStatus(Integer statusId){
        return reimbursementDao.viewAllReimbursementByStatus(statusId);
    }
}
