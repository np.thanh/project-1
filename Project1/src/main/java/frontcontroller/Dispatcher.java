package frontcontroller;


import controller.EmployeeController;
import controller.ManagerController;
import controller.UserController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//Endpoint routing
@WebServlet(name="dispatcher", urlPatterns = "/api/*")
public class Dispatcher extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String URI = req.getRequestURI();
        System.out.println(URI);

        switch (URI){
            case "/Project1/api/login":
                if(req.getMethod().equals("POST"))
                    UserController.getInstance().login(req, resp);
                break;
            case "/Project1/api/register":
                if (req.getMethod().equals("POST"))
                    UserController.getInstance().register(req, resp);
                break;
            case "/Project1/api/employee":
                switch (req.getMethod()){
                    case "GET":
                        EmployeeController.getInstance().employeeGetAllTickets(req, resp);
                        break;
                    case "POST":
                        EmployeeController.getInstance().employeeAddReimbursement(req, resp);
                        break;
                } break;
            case "/Project1/api/manager":
                switch (req.getMethod()){
                    case "GET":
                        ManagerController.getInstance().managerViewAllReimbursement(req, resp);
                        break;
                    case "PATCH":
                        ManagerController.getInstance().managerUpdateStatus(req, resp);
                        break;
                } break;
            case "/Project1/api/manager/view-request-by-status":
                if(req.getMethod().equals("GET"))
                    ManagerController.getInstance().managerViewReimbursementByStatus(req, resp);
                break;
            case "/Project1/api/get-all-users":
                if(req.getMethod().equals("GET"))
                    UserController.getInstance().getAllUser(req, resp);
                break;
            case "/Project1/api/check-session":
                if(req.getMethod().equals("GET"))
                    UserController.getInstance().checkSession(req, resp);
                break;
            case "/Project1/api/logout":
                if(req.getMethod().equals("GET"))
                    UserController.getInstance().logout(req, resp);
                break;
        }
    }
}
