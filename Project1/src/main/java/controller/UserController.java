package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Response;
import model.User;
import service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

public class UserController {
    private static UserController userController;
    UserService userService;
    private UserController(){userService = new UserService();
    }

    public static UserController getInstance(){
        if (userController == null)
            userController = new UserController();
        return userController;
    }

    public void login(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

        User user = new ObjectMapper().readValue(requestBody, User.class);

        User checkingUser = userService.getUser(user);

        if (checkingUser != null){
            //create session if successful
            HttpSession httpSession = req.getSession(true);
            httpSession.setAttribute("user", checkingUser);

            out.println(new ObjectMapper().writeValueAsString(new Response("Login successful", true, checkingUser)));

        }else{
            out.println(new ObjectMapper().writeValueAsString(new Response("Invalid username or password", false, null)));
        }
    }



    public void register(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        User user = new ObjectMapper().readValue(requestBody, User.class);

        User checkingUser = userService.addUser(user);

        if(checkingUser != null){
            out.println(new ObjectMapper().writeValueAsString(new Response("Account was created", true, checkingUser)));

        }else{
            out.println(new ObjectMapper().writeValueAsString(new Response("Username already existed", false, null)));
        }
    }

    public void getAllUser(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        List<User> users = userService.getAllUser();

        if(users != null){
            out.println(new ObjectMapper().writeValueAsString(new Response("List of all users", true, users)));

        }else{
            out.println(new ObjectMapper().writeValueAsString(new Response("No users in database", false, null)));
        }
    }

    public void checkSession(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();
        User user = (User) req.getSession().getAttribute("user");

        if(user != null){
            out.println(new ObjectMapper().writeValueAsString(new Response("session found", true, user)));
        }else{
            out.println(new ObjectMapper().writeValueAsString(new Response("no session", false, null)));
        }
    }

    public void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        req.getSession().setAttribute("user", null);

        out.println(new ObjectMapper().writeValueAsString(new Response("session terminated",true,null)));
    }
}
