package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Response;
import service.ManagerService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ManagerController {
    public static ManagerController managerController;
    ManagerService managerService;

    private ManagerController(){
        managerService = new ManagerService();
    }

    public static ManagerController getInstance(){
        if (managerController == null)
            managerController = new ManagerController();
        return managerController;
    }

    public void managerViewAllReimbursement(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        out.println(new ObjectMapper().writeValueAsString(new Response("View all manager reimbursements", true, managerService.managerViewAllReimbursement())));
    }

    public void managerViewReimbursementByStatus(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        Integer statusId = Integer.parseInt(req.getParameter("statusId"));

        out.println(new ObjectMapper().writeValueAsString(new Response("View all reimbursements by status", true, managerService.managerViewAllRequestByStatus(statusId))));
    }

    public void managerUpdateStatus(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        Integer reImbId = Integer.parseInt(req.getParameter("id"));
        Integer statusId = Integer.parseInt(req.getParameter("statusId"));
        Integer resolverId = Integer.parseInt(req.getParameter("resolverId"));

        managerService.managerUpdateStatus(reImbId, statusId, resolverId);
        out.println(new ObjectMapper().writeValueAsString(new Response("Status updated.", true, null)));


    }


}
