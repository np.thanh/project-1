package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Reimbursement;
import model.Response;
import service.EmployeeService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

public class EmployeeController{
    private static EmployeeController employeeController;
    EmployeeService employeeService;
    private EmployeeController(){
        employeeService = new EmployeeService();
    }

    public static EmployeeController getInstance(){
        if (employeeController == null)
            employeeController = new EmployeeController();
        return
                employeeController;
    }

    public void employeeGetAllTickets(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        Integer employeeId = Integer.parseInt(req.getParameter("id"));

        out.println(new ObjectMapper().writeValueAsString(new Response("View all Employee reimbursements", true, employeeService.employeeViewReimbursement(employeeId))));

    }

    public void employeeAddReimbursement(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

        Reimbursement reimbursement = new ObjectMapper().readValue(requestBody, Reimbursement.class);

        employeeService.addReimbursementRequest(reimbursement);

        out.println(new ObjectMapper().writeValueAsString(new Response("New Reimbursement has been created.", true, null)));
    }
}
