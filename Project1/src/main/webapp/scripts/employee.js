const urlParams = new URLSearchParams(window.location.search)
const userId = urlParams.get("id")
 
 window.onload = async function(){
//check session
     const sessionRes = await fetch(`${domain}/api/check-session`)
    const sessionDataRes = await sessionRes.json()
    console.log(sessionDataRes)


    if(sessionDataRes.success){
        if(sessionDataRes.data.userID != userId){
            window.location = `${domain}/`
        } 
     }else{
           window.location = `${domain}/`
     }
 
     viewEmployee()
}

async function viewEmployee(){

//Create all the buttons
let employeeDisplayElem = document.getElementById("employee-display")

let pastTicketBtn = document.createElement("button")
pastTicketBtn.id = "past-ticket"
pastTicketBtn.innerText = "View Past Tickets"

let pendingBtn = document.createElement("button")
pendingBtn.id = "pending-ticket"
pendingBtn.innerText = "View Pending Requests"

let addReimbursementBtn = document.createElement("button")
addReimbursementBtn.id = "add-reimbursement"
addReimbursementBtn.innerText = "Add Reimbursement Request"

employeeDisplayElem.appendChild(pastTicketBtn)
employeeDisplayElem.appendChild(pendingBtn)
employeeDisplayElem.appendChild(addReimbursementBtn)

let ticketCardEle = document.getElementById("ticket-card")


//Employee view all the tickets
pastTicketBtn.onclick = async function(){
    let ticketRes = await fetch(`${domain}/api/employee?id=${userId}`, {
       method: "GET"
   })

   let ticketDataResp = await ticketRes.json()
   console.log(ticketDataResp)

     //sort the data
     ticketDataResp.data.sort((a,b) => {
        if(a.reimbursementID < b.reimbursementID)
            return 1
        if(a.reimbursementID > b.reimbursementID)
            return -1;
        
        return 0;    
    })



   //Get all users to display employee name and resolver name
   let allUserRes = await fetch (`${domain}/api/get-all-users`, {
    method: "GET"   
    })
    let allUserDataRes = await allUserRes.json()
    console.log(allUserDataRes)
    
    
    reimbursementForm.innerHTML='';
   ticketCardEle.innerHTML=''; 

   
   ticketDataResp.data.forEach(list => {
          
  

    let status
   if(list.reimbursementStatusID_FK == 1){status = "Pending"}
   else if (list.reimbursementStatusID_FK == 2){status = "Approved"}
   else if (list.reimbursementStatusID_FK == 3){status = "Denied"}

   let type
   if(list.reimbursementTypeID_FK == 1){type = "Lodging"}
   else if(list.reimbursementTypeID_FK == 2){type = "Travel"}
   else if(list.reimbursementTypeID_FK == 3){type = "Food"}
   else if(list.reimbursementTypeID_FK == 4){type = "Other"}

   
    let cardEle = document.createElement("div");
    cardEle.id= "card";

   let reimbursementIdEle = document.createElement("p");
   reimbursementIdEle.id = "reimbursementId";
   reimbursementIdEle.innerText = "Reimbursement ID: " + list.reimbursementID;

   let reimbursementAmountEle = document.createElement("p");
   reimbursementAmountEle.id = "reimbursementAmount";
   reimbursementAmountEle.innerText = "Reimbursement Amount: " + list.reimbursementAmount;

   let reimbursementSubmittedEle = document.createElement("p");
   reimbursementSubmittedEle.id = "reimbursementSubmitted";
   reimbursementSubmittedEle.innerText = "Reimbursement Submitted: " + list.reimbursementSubmitted;

   let reimbursementResolvedEle = document.createElement("p");
   reimbursementResolvedEle.id = "reimbursementResolved";
   reimbursementResolvedEle.innerText = "Reimbursement Resolved: " + list.reimbursementResolved;

   let reimbursementDescriptionEle = document.createElement("p");
   reimbursementDescriptionEle.id = "reimbursementDescription";
   reimbursementDescriptionEle.innerText = "Reimbursement Description: " + list.reimbursementDescription;

   let resolver
   allUserDataRes.data.forEach(userList => {
        if (list.reimbursementResolver == userList.userID){
           resolver = userList.firstName
        }
   })

   let reimbursementResolverEle = document.createElement("p");
   reimbursementResolverEle.id = "reimbursementResolver";
   reimbursementResolverEle.innerText = "Reimbursement Resolver: " + resolver;

   let reimbursementStatusID_FKEle = document.createElement("p");
   reimbursementStatusID_FKEle.id = "reimbursementStatusID_FK";
   reimbursementStatusID_FKEle.innerText = "Reimbursement Status: " + status;

   let reimbursementTypeID_FKEle = document.createElement("p");
   reimbursementTypeID_FKEle.id = "reimbursementTypeID_FK";
   reimbursementTypeID_FKEle.innerText = "Reimbursement Type: " + type;

   cardEle.appendChild(reimbursementIdEle);
   cardEle.appendChild(reimbursementAmountEle);
   cardEle.appendChild(reimbursementSubmittedEle);
   cardEle.appendChild(reimbursementResolvedEle);
   cardEle.appendChild(reimbursementDescriptionEle);
   cardEle.appendChild(reimbursementResolverEle);
   cardEle.appendChild(reimbursementStatusID_FKEle);
   cardEle.appendChild(reimbursementTypeID_FKEle);

   ticketCardEle.appendChild(cardEle)

   })  
   
}  


//Employee view all pending tickets
pendingBtn.onclick = async function(){
    let ticketRes = await fetch(`${domain}/api/employee?id=${userId}`, {
       method: "GET"
   })
   let ticketDataResp = await ticketRes.json()
   console.log(ticketDataResp)

   //sort the data
   ticketDataResp.data.sort((a,b) => {
    if(a.reimbursementID < b.reimbursementID)
        return 1
    if(a.reimbursementID > b.reimbursementID)
        return -1;
    
    return 0;    
})

   //Get all users to display employee name and resolver name
   let allUserRes = await fetch (`${domain}/api/get-all-users`, {
    method: "GET"   
    })
    let allUserDataRes = await allUserRes.json()
    console.log(allUserDataRes)
    


    reimbursementForm.innerHTML='';
   ticketCardEle.innerHTML=''; 

   ticketDataResp.data.forEach(list => {

    let status
   if(list.reimbursementStatusID_FK == 1){status = "Pending"}
   else if (list.reimbursementStatusID_FK == 2){status = "Approved"}
   else if (list.reimbursementStatusID_FK == 3){status = "Denied"}

   let type
   if(list.reimbursementTypeID_FK == 1){type = "Lodging"}
   else if(list.reimbursementTypeID_FK == 2){type = "Travel"}
   else if(list.reimbursementTypeID_FK == 3){type = "Food"}
   else if(list.reimbursementTypeID_FK == 4){type = "Other"}
   
       let cardEle = document.createElement("div");
       cardEle.id= "card";

       if(list.reimbursementStatusID_FK == '1'){

   let reimbursementIdEle = document.createElement("p");
   reimbursementIdEle.id = "reimbursementId";
   reimbursementIdEle.innerText = "Reimbursement ID: " + list.reimbursementID;

   let reimbursementAmountEle = document.createElement("p");
   reimbursementAmountEle.id = "reimbursementAmount";
   reimbursementAmountEle.innerText = "Reimbursement Amount: " + list.reimbursementAmount;

   let reimbursementSubmittedEle = document.createElement("p");
   reimbursementSubmittedEle.id = "reimbursementSubmitted";
   reimbursementSubmittedEle.innerText = "Reimbursement Submitted: " + list.reimbursementSubmitted;

   let reimbursementResolvedEle = document.createElement("p");
   reimbursementResolvedEle.id = "reimbursementResolved";
   reimbursementResolvedEle.innerText = "Reimbursement Resolved: " + list.reimbursementResolved;

   let reimbursementDescriptionEle = document.createElement("p");
   reimbursementDescriptionEle.id = "reimbursementDescription";
   reimbursementDescriptionEle.innerText = "Reimbursement Description: " + list.reimbursementDescription;

   
   let resolver
   allUserDataRes.data.forEach(userList => {
        if (list.reimbursementResolver == userList.userID){
           resolver = userList.firstName
        }
   })

   let reimbursementResolverEle = document.createElement("p");
   reimbursementResolverEle.id = "reimbursementResolver";
   reimbursementResolverEle.innerText = "Reimbursement Resolver: " + resolver;

   let reimbursementStatusID_FKEle = document.createElement("p");
   reimbursementStatusID_FKEle.id = "reimbursementStatusID_FK";
   reimbursementStatusID_FKEle.innerText = "Reimbursement Status: " + status;

   let reimbursementTypeID_FKEle = document.createElement("p");
   reimbursementTypeID_FKEle.id = "reimbursementTypeID_FK";
   reimbursementTypeID_FKEle.innerText = "Reimbursement Type: " + type;

   cardEle.appendChild(reimbursementIdEle);
   cardEle.appendChild(reimbursementAmountEle);
   cardEle.appendChild(reimbursementSubmittedEle);
   cardEle.appendChild(reimbursementResolvedEle);
   cardEle.appendChild(reimbursementDescriptionEle);
   cardEle.appendChild(reimbursementResolverEle);
   cardEle.appendChild(reimbursementStatusID_FKEle);
   cardEle.appendChild(reimbursementTypeID_FKEle);
       
   ticketCardEle.appendChild(cardEle)
       }

   })  
   
} 


//Form to add new reimbursement
var reimbursementForm = document.getElementById("reimbursement-form")
addReimbursementBtn.onclick = async function(){
    reimbursementForm.innerHTML='';
    ticketCardEle.innerHTML='';

    reimbursementForm.innerHTML = 
        `<form id="create-reimbursement">
        <div class="container">
               <label for="amount">Amount: </label><br>
               <input type="text" id="amount"><br>

               <label for="description">Description: </label><br>
               <input type="text" id="description"><br>


               <lable for="type">Choose a type: </lable><br>
               <select id="type">
                   <option value="">--Please choose an option--</option>
                   <option value="Lodging">Lodging</option>
                   <option value="Travel">Travel</option>
                   <option value="Food">Food</option>
                   <option value="Other">Other</option>>
               </select><br>

               <button type="submit" id="new-submit">Create</button>
           </div> 
           <div id="response-message"></div>
       </form>`

    let submitBtn = document.getElementById("new-submit")
    submitBtn.onclick = function(e){
        e.preventDefault();

        addNewReimbursement();
    }

    async function addNewReimbursement(){
        let amount = document.getElementById("amount").value;
        let description = document.getElementById("description").value;
        let type = document.getElementById("type").value;
        let typeValue;
        if(type=="Lodging"){typeValue = 1}
        else if (type=="Travel"){typeValue = 2}
        else if (type=="Food"){typeValue = 3}
        else if (type=="Other"){typeValue=4}

        let response = await fetch(`${domain}/api/employee?id=${userId}`, {
            method: "POST",
            body: JSON.stringify(
                {
                    reimbursementAmount: amount,
                    reimbursementDescription: description,
                    reimbursementAuthor: userId,
                    reimbursementStatusID_FK: "1",
                    reimbursementTypeID_FK: typeValue

                }
            )
        })

        let responseData = await response.json();
        console.log(responseData); 

        if(responseData.success){
            let messageElem = document.getElementById("response-message")
            messageElem.style = "background-color: white;"
            messageElem.innerText = responseData.message
         }
    }
}
}

//end session and redirect to login when logout button is clicked
let logoutBtn = document.getElementById("logout-btn")
logoutBtn.onclick = async function(){
    let logoutRes = await fetch(`${domain}/api/logout`)

    let logoutResData = await logoutRes.json();

    if(logoutResData.success)
        window.location = `${domain}/`
}


