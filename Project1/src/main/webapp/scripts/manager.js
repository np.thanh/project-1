const urlParams = new URLSearchParams(window.location.search)
const userId = urlParams.get("id")
const statusId = urlParams.get("statusId")

window.onload = async function(){
//check session
 const sessionRes = await fetch(`${domain}/api/check-session`)
const sessionDataRes = await sessionRes.json()
console.log(sessionDataRes)

if(sessionDataRes.success){
    if(sessionDataRes.data.userID != userId){
        window.location = `${domain}/`
    } 
 }else{
     window.location = `${domain}/`
 } 

    viewManager()
}


//view Manager page when the window onload
async function viewManager(){

    //create buttons

    let managerDisplayElem = document.getElementById("manager-display")

    let viewAllBtn = document.createElement("button")
    viewAllBtn.id = "all-reimbursement"
    viewAllBtn.innerText = "View All Reimbursement"

    let viewByStatusBtn = document.createElement("button")
    viewByStatusBtn.id = "view-status"
    viewByStatusBtn.innerText = "View Reimbursement By Status"

    managerDisplayElem.appendChild(viewAllBtn)
    managerDisplayElem.appendChild(viewByStatusBtn)

    let managerCardEle = document.getElementById("reimbursement-card")

 
    //ViewAllButtion onclick
    viewAllBtn.onclick = async function(){
        viewAllFunction()
    }
    
    async function viewAllFunction(){
        let ticketRes = await fetch(`${domain}/api/manager`, {
           method: "GET"
       })
    
       let ticketDataResp = await ticketRes.json()
       console.log(ticketDataResp)

        //sort the data
        ticketDataResp.data.sort((a,b) => {
            if(a.reimbursementID < b.reimbursementID)
                return 1
            if(a.reimbursementID > b.reimbursementID)
                return -1;
        
            return 0;    
        })


       //Get all users to display employee name and resolver name
        let allUserRes = await fetch (`${domain}/api/get-all-users`, {
        method: "GET"   
        })
        let allUserDataRes = await allUserRes.json()
        console.log(allUserDataRes)
    
        managerCardEle.innerHTML =''
    
       ticketDataResp.data.forEach(list => {

        //Approved, Denied, Pending button
        let btnContEle = document.createElement("div")
        btnContEle.id = "btn-container"

        let pendingBtnEle = document.createElement("button")
        pendingBtnEle.id = "pendingBtn"
        pendingBtnEle.innerText = "Pending"

        let approvedBtnEle = document.createElement("button")
        approvedBtnEle.id = "approvedBtn"
        approvedBtnEle.innerText = "Approve"

        let deniedBtnEle = document.createElement("button")
        deniedBtnEle.id = "deniedBtn"
        deniedBtnEle.innerText = "Deny"

        let status
        if(list.reimbursementStatusID_FK == 1)
         {status = "Pending"
            btnContEle.appendChild(approvedBtnEle)
            btnContEle.appendChild(deniedBtnEle)
        }

        else if (list.reimbursementStatusID_FK == 2)
        {status = "Approved"
            btnContEle.appendChild(pendingBtnEle)
            btnContEle.appendChild(deniedBtnEle) 
        }

        else if (list.reimbursementStatusID_FK == 3)
        {status = "Denied"
         btnContEle.appendChild(pendingBtnEle)
        btnContEle.appendChild(approvedBtnEle) 
        }

        let type
        if(list.reimbursementTypeID_FK == 1){type = "Lodging"}
        else if(list.reimbursementTypeID_FK == 2){type = "Travel"}
        else if(list.reimbursementTypeID_FK == 3){type = "Food"}
        else if(list.reimbursementTypeID_FK == 4){type = "Other"}

        let cardEle = document.createElement("div");
        cardEle.id= "card";
 
    let reimbursementIdEle = document.createElement("p");
    reimbursementIdEle.id = "reimbursementId";
    reimbursementIdEle.innerText = "Reimbursement ID: " + list.reimbursementID;
 
    let reimbursementAmountEle = document.createElement("p");
    reimbursementAmountEle.id = "reimbursementAmount";
    reimbursementAmountEle.innerText = "Reimbursement Amount: " + list.reimbursementAmount;
 
    let reimbursementSubmittedEle = document.createElement("p");
    reimbursementSubmittedEle.id = "reimbursementSubmitted";
    reimbursementSubmittedEle.innerText = "Reimbursement Submitted: " + list.reimbursementSubmitted;
 
    let reimbursementResolvedEle = document.createElement("p");
    reimbursementResolvedEle.id = "reimbursementResolved";
    reimbursementResolvedEle.innerText = "Reimbursement Resolved: " + list.reimbursementResolved;
 
    let reimbursementDescriptionEle = document.createElement("p");
    reimbursementDescriptionEle.id = "reimbursementDescription";
    reimbursementDescriptionEle.innerText = "Reimbursement Description: " + list.reimbursementDescription;
 
    let reimbursementReceiptEle = document.createElement("p");
    reimbursementReceiptEle.id = "reimbursementReceipt";
    reimbursementReceiptEle.innerText = "Reimbursement Receipt: " + list.reimbursementReceipt;
 

    let author
    allUserDataRes.data.forEach(userList => {
       if (list.reimbursementAuthor == userList.userID){
          author = userList.firstName
       }
   })
    let reimbursementAuthorEle = document.createElement("p");
    reimbursementAuthorEle.id = "reimbursementAuthor";
    reimbursementAuthorEle.innerText = "Reimbursement Author: " + author;

    let resolver
     allUserDataRes.data.forEach(userList => {
        if (list.reimbursementResolver == userList.userID){
           resolver = userList.firstName
        }
    })
 
    let reimbursementResolverEle = document.createElement("p");
    reimbursementResolverEle.id = "reimbursementResolver";
    reimbursementResolverEle.innerText = "Reimbursement Resolver: " + resolver;
 
    let reimbursementStatusID_FKEle = document.createElement("p");
    reimbursementStatusID_FKEle.id = "reimbursementStatusID_FK";
    reimbursementStatusID_FKEle.innerText = "Reimbursement Status: " + status;
 
    let reimbursementTypeID_FKEle = document.createElement("p");
    reimbursementTypeID_FKEle.id = "reimbursementTypeID_FK";
    reimbursementTypeID_FKEle.innerText = "Reimbursement Type: " + type;
 
    cardEle.appendChild(reimbursementIdEle);
    cardEle.appendChild(reimbursementAmountEle);
    cardEle.appendChild(reimbursementSubmittedEle);
    cardEle.appendChild(reimbursementResolvedEle);
    cardEle.appendChild(reimbursementDescriptionEle);
    cardEle.appendChild(reimbursementReceiptEle);
    cardEle.appendChild(reimbursementAuthorEle);
    cardEle.appendChild(reimbursementResolverEle);
    cardEle.appendChild(reimbursementStatusID_FKEle);
    cardEle.appendChild(reimbursementTypeID_FKEle);
    cardEle.appendChild(btnContEle);
 
    managerCardEle.appendChild(cardEle);


    //Pending button onclick
    pendingBtnEle.onclick = async function(){
        let pendingRes = await fetch(`${domain}/api/manager?id=${list.reimbursementID}&statusId=1&resolverId=${userId}`, {
            method: "PATCH"
        })
        let pendingDataRes = await pendingRes.json()
        console.log(pendingDataRes)
        if(pendingDataRes.success){
            viewAllFunction()
        }

    }

    //Approve button onclick
    approvedBtnEle.onclick = async function(){
        let approvedRes = await fetch(`${domain}/api/manager?id=${list.reimbursementID}&statusId=2&resolverId=${userId}`, {
            method: "PATCH"
        })
        let approvedDataRes = await approvedRes.json()
        console.log(approvedDataRes)
        if(approvedDataRes.success){
            viewAllFunction()
        }
    }

    //Deny button onclick
    deniedBtnEle.onclick = async function(){
        let deniedRes = await fetch(`${domain}/api/manager?id=${list.reimbursementID}&statusId=3&resolverId=${userId}`, {
            method: "PATCH"
        })
        let deniedDataRes = await deniedRes.json()
        console.log(deniedDataRes)
        if(deniedDataRes.success){
            viewAllFunction()
        }
    }
 
       
})  
    }  


    //view by status onclick
    viewByStatusBtn.onclick = async function(){
        managerCardEle.innerHTML =''
        let status = document.createElement("div")
        status.id= "status-option"

        let pending = document.createElement("button")
        pending.id = ("pending")
        pending.innerText = "Pending"

        let approved = document.createElement("button")
        approved.id = ("approved")
        approved.innerText = "Approved"

        let denied = document.createElement("button")
        denied.id = ("denied")
        denied.innerText = "Denied"

        status.appendChild(pending)
        status.appendChild(approved)
        status.appendChild(denied)

        managerCardEle.appendChild(status)

        //pending onclick
        pending.onclick = async function(){
            let ticketRes = await fetch(`${domain}/api/manager`, {
                method: "GET"
            })
         
            let ticketDataResp = await ticketRes.json()
            console.log(ticketDataResp)

            //sort the data
     ticketDataResp.data.sort((a,b) => {
        if(a.reimbursementID < b.reimbursementID)
            return 1
        if(a.reimbursementID > b.reimbursementID)
            return -1;
        
        return 0;    
    })

            //Get all users to display employee name and resolver name
        let allUserRes = await fetch (`${domain}/api/get-all-users`, {
            method: "GET"   
            })
            let allUserDataRes = await allUserRes.json()
            console.log(allUserDataRes)
         
             managerCardEle.innerHTML =''
         
            ticketDataResp.data.forEach(list => {

                let status
                if(list.reimbursementStatusID_FK == 1){status = "Pending"}
                else if (list.reimbursementStatusID_FK == 2){status = "Approved"}
                else if (list.reimbursementStatusID_FK == 3){status = "Denied"}
                let type
                if(list.reimbursementTypeID_FK == 1){type = "Lodging"}
                else if(list.reimbursementTypeID_FK == 2){type = "Travel"}
                else if(list.reimbursementTypeID_FK == 3){type = "Food"}
                else if(list.reimbursementTypeID_FK == 4){type = "Other"}

            
             let cardEle = document.createElement("div");
             cardEle.id= "card";

             if(list.reimbursementStatusID_FK == "1"){
      
         let reimbursementIdEle = document.createElement("p");
         reimbursementIdEle.id = "reimbursementId";
         reimbursementIdEle.innerText = "Reimbursement ID: " + list.reimbursementID;
      
         let reimbursementAmountEle = document.createElement("p");
         reimbursementAmountEle.id = "reimbursementAmount";
         reimbursementAmountEle.innerText = "Reimbursement Amount: " + list.reimbursementAmount;
      
         let reimbursementSubmittedEle = document.createElement("p");
         reimbursementSubmittedEle.id = "reimbursementSubmitted";
         reimbursementSubmittedEle.innerText = "Reimbursement Submitted: " + list.reimbursementSubmitted;
      
         let reimbursementResolvedEle = document.createElement("p");
         reimbursementResolvedEle.id = "reimbursementResolved";
         reimbursementResolvedEle.innerText = "Reimbursement Resolved: " + list.reimbursementResolved;
      
         let reimbursementDescriptionEle = document.createElement("p");
         reimbursementDescriptionEle.id = "reimbursementDescription";
         reimbursementDescriptionEle.innerText = "Reimbursement Description: " + list.reimbursementDescription;
      
         let reimbursementReceiptEle = document.createElement("p");
         reimbursementReceiptEle.id = "reimbursementReceipt";
         reimbursementReceiptEle.innerText = "Reimbursement Receipt: " + list.reimbursementReceipt;
      

         let author
        allUserDataRes.data.forEach(userList => {
       if (list.reimbursementAuthor == userList.userID){
          author = userList.firstName
       }
         })
         let reimbursementAuthorEle = document.createElement("p");
         reimbursementAuthorEle.id = "reimbursementAuthor";
         reimbursementAuthorEle.innerText = "Reimbursement Author: " + author;

         let resolver
        allUserDataRes.data.forEach(userList => {
        if (list.reimbursementResolver == userList.userID){
           resolver = userList.firstName
        }
         })
      
         let reimbursementResolverEle = document.createElement("p");
         reimbursementResolverEle.id = "reimbursementResolver";
         reimbursementResolverEle.innerText = "Reimbursement Resolver: " + resolver;
      
         let reimbursementStatusID_FKEle = document.createElement("p");
         reimbursementStatusID_FKEle.id = "reimbursementStatusID_FK";
         reimbursementStatusID_FKEle.innerText = "Reimbursement Status: " + status;
      
         let reimbursementTypeID_FKEle = document.createElement("p");
         reimbursementTypeID_FKEle.id = "reimbursementTypeID_FK";
         reimbursementTypeID_FKEle.innerText = "Reimbursement Type: " + type;
      
         cardEle.appendChild(reimbursementIdEle);
         cardEle.appendChild(reimbursementAmountEle);
         cardEle.appendChild(reimbursementSubmittedEle);
         cardEle.appendChild(reimbursementResolvedEle);
         cardEle.appendChild(reimbursementDescriptionEle);
         cardEle.appendChild(reimbursementReceiptEle);
         cardEle.appendChild(reimbursementAuthorEle);
         cardEle.appendChild(reimbursementResolverEle);
         cardEle.appendChild(reimbursementStatusID_FKEle);
         cardEle.appendChild(reimbursementTypeID_FKEle);
      
         managerCardEle.appendChild(cardEle)
                } 
     })
        }

        //approved onclick
        approved.onclick = async function(){
            let ticketRes = await fetch(`${domain}/api/manager`, {
                method: "GET"
            })
         
            let ticketDataResp = await ticketRes.json()
            console.log(ticketDataResp)

            //sort the data
     ticketDataResp.data.sort((a,b) => {
        if(a.reimbursementID < b.reimbursementID)
            return 1
        if(a.reimbursementID > b.reimbursementID)
            return -1;
        
        return 0;    
    })

            //Get all users to display employee name and resolver name
        let allUserRes = await fetch (`${domain}/api/get-all-users`, {
            method: "GET"   
            })
            let allUserDataRes = await allUserRes.json()
            console.log(allUserDataRes)
         
             managerCardEle.innerHTML =''
         
            ticketDataResp.data.forEach(list => {

                let status
                if(list.reimbursementStatusID_FK == 1){status = "Pending"}
                else if (list.reimbursementStatusID_FK == 2){status = "Approved"}
                else if (list.reimbursementStatusID_FK == 3){status = "Denied"}
                let type
                if(list.reimbursementTypeID_FK == 1){type = "Lodging"}
                else if(list.reimbursementTypeID_FK == 2){type = "Travel"}
                else if(list.reimbursementTypeID_FK == 3){type = "Food"}
                else if(list.reimbursementTypeID_FK == 4){type = "Other"}
            
             let cardEle = document.createElement("div");
             cardEle.id= "card";

             if(list.reimbursementStatusID_FK == "2"){
      
         let reimbursementIdEle = document.createElement("p");
         reimbursementIdEle.id = "reimbursementId";
         reimbursementIdEle.innerText = "Reimbursement ID: " + list.reimbursementID;
      
         let reimbursementAmountEle = document.createElement("p");
         reimbursementAmountEle.id = "reimbursementAmount";
         reimbursementAmountEle.innerText = "Reimbursement Amount: " + list.reimbursementAmount;
      
         let reimbursementSubmittedEle = document.createElement("p");
         reimbursementSubmittedEle.id = "reimbursementSubmitted";
         reimbursementSubmittedEle.innerText = "Reimbursement Submitted: " + list.reimbursementSubmitted;
      
         let reimbursementResolvedEle = document.createElement("p");
         reimbursementResolvedEle.id = "reimbursementResolved";
         reimbursementResolvedEle.innerText = "Reimbursement Resolved: " + list.reimbursementResolved;
      
         let reimbursementDescriptionEle = document.createElement("p");
         reimbursementDescriptionEle.id = "reimbursementDescription";
         reimbursementDescriptionEle.innerText = "Reimbursement Description: " + list.reimbursementDescription;
      
         let reimbursementReceiptEle = document.createElement("p");
         reimbursementReceiptEle.id = "reimbursementReceipt";
         reimbursementReceiptEle.innerText = "Reimbursement Receipt: " + list.reimbursementReceipt;
      

         let author
         allUserDataRes.data.forEach(userList => {
            if (list.reimbursementAuthor == userList.userID){
               author = userList.firstName
            }
        })
         let reimbursementAuthorEle = document.createElement("p");
         reimbursementAuthorEle.id = "reimbursementAuthor";
         reimbursementAuthorEle.innerText = "Reimbursement Author: " + author;
      
         let resolver
        allUserDataRes.data.forEach(userList => {
        if (list.reimbursementResolver == userList.userID){
           resolver = userList.firstName
        }
        })
         let reimbursementResolverEle = document.createElement("p");
         reimbursementResolverEle.id = "reimbursementResolver";
         reimbursementResolverEle.innerText = "Reimbursement Resolver: " + resolver;
      
         let reimbursementStatusID_FKEle = document.createElement("p");
         reimbursementStatusID_FKEle.id = "reimbursementStatusID_FK";
         reimbursementStatusID_FKEle.innerText = "Reimbursement Status: " + status;
      
         let reimbursementTypeID_FKEle = document.createElement("p");
         reimbursementTypeID_FKEle.id = "reimbursementTypeID_FK";
         reimbursementTypeID_FKEle.innerText = "Reimbursement Type: " + type;
      
         cardEle.appendChild(reimbursementIdEle);
         cardEle.appendChild(reimbursementAmountEle);
         cardEle.appendChild(reimbursementSubmittedEle);
         cardEle.appendChild(reimbursementResolvedEle);
         cardEle.appendChild(reimbursementDescriptionEle);
         cardEle.appendChild(reimbursementReceiptEle);
         cardEle.appendChild(reimbursementAuthorEle);
         cardEle.appendChild(reimbursementResolverEle);
         cardEle.appendChild(reimbursementStatusID_FKEle);
         cardEle.appendChild(reimbursementTypeID_FKEle);
      
         managerCardEle.appendChild(cardEle)
                } 
     })
        }

        //denied onclick
        denied.onclick = async function(){
            let ticketRes = await fetch(`${domain}/api/manager`, {
                method: "GET"
            })
         
            let ticketDataResp = await ticketRes.json()
            console.log(ticketDataResp)

            //sort the data
     ticketDataResp.data.sort((a,b) => {
        if(a.reimbursementID < b.reimbursementID)
            return 1
        if(a.reimbursementID > b.reimbursementID)
            return -1;
        
        return 0;    
    })

            //Get all users to display employee name and resolver name
        let allUserRes = await fetch (`${domain}/api/get-all-users`, {
            method: "GET"   
            })
            let allUserDataRes = await allUserRes.json()
            console.log(allUserDataRes)
         
             managerCardEle.innerHTML =''
         
            ticketDataResp.data.forEach(list => {
                let status
                if(list.reimbursementStatusID_FK == 1){status = "Pending"}
                else if (list.reimbursementStatusID_FK == 2){status = "Approved"}
                else if (list.reimbursementStatusID_FK == 3){status = "Denied"}
                let type
                if(list.reimbursementTypeID_FK == 1){type = "Lodging"}
                else if(list.reimbursementTypeID_FK == 2){type = "Travel"}
                else if(list.reimbursementTypeID_FK == 3){type = "Food"}
                else if(list.reimbursementTypeID_FK == 4){type = "Other"}
            
             let cardEle = document.createElement("div");
             cardEle.id= "card";
      
             if(list.reimbursementStatusID_FK == "3"){

         let reimbursementIdEle = document.createElement("p");
         reimbursementIdEle.id = "reimbursementId";
         reimbursementIdEle.innerText = "Reimbursement ID: " + list.reimbursementID;
      
         let reimbursementAmountEle = document.createElement("p");
         reimbursementAmountEle.id = "reimbursementAmount";
         reimbursementAmountEle.innerText = "Reimbursement Amount: " + list.reimbursementAmount;
      
         let reimbursementSubmittedEle = document.createElement("p");
         reimbursementSubmittedEle.id = "reimbursementSubmitted";
         reimbursementSubmittedEle.innerText = "Reimbursement Submitted: " + list.reimbursementSubmitted;
      
         let reimbursementResolvedEle = document.createElement("p");
         reimbursementResolvedEle.id = "reimbursementResolved";
         reimbursementResolvedEle.innerText = "Reimbursement Resolved: " + list.reimbursementResolved;
      
         let reimbursementDescriptionEle = document.createElement("p");
         reimbursementDescriptionEle.id = "reimbursementDescription";
         reimbursementDescriptionEle.innerText = "Reimbursement Description: " + list.reimbursementDescription;
      
         let reimbursementReceiptEle = document.createElement("p");
         reimbursementReceiptEle.id = "reimbursementReceipt";
         reimbursementReceiptEle.innerText = "Reimbursement Receipt: " + list.reimbursementReceipt;
      
         let author
        allUserDataRes.data.forEach(userList => {
       if (list.reimbursementAuthor == userList.userID){
          author = userList.firstName
       }
         })
         let reimbursementAuthorEle = document.createElement("p");
         reimbursementAuthorEle.id = "reimbursementAuthor";
         reimbursementAuthorEle.innerText = "Reimbursement Author: " + author;
      
         let resolver
        allUserDataRes.data.forEach(userList => {
        if (list.reimbursementResolver == userList.userID){
           resolver = userList.firstName
        }
        })
         let reimbursementResolverEle = document.createElement("p");
         reimbursementResolverEle.id = "reimbursementResolver";
         reimbursementResolverEle.innerText = "Reimbursement Resolver: " + resolver;
      
         let reimbursementStatusID_FKEle = document.createElement("p");
         reimbursementStatusID_FKEle.id = "reimbursementStatusID_FK";
         reimbursementStatusID_FKEle.innerText = "Reimbursement Status: " + status;
      
         let reimbursementTypeID_FKEle = document.createElement("p");
         reimbursementTypeID_FKEle.id = "reimbursementTypeID_FK";
         reimbursementTypeID_FKEle.innerText = "Reimbursement Type: " + type;
      
         cardEle.appendChild(reimbursementIdEle);
         cardEle.appendChild(reimbursementAmountEle);
         cardEle.appendChild(reimbursementSubmittedEle);
         cardEle.appendChild(reimbursementResolvedEle);
         cardEle.appendChild(reimbursementDescriptionEle);
         cardEle.appendChild(reimbursementReceiptEle);
         cardEle.appendChild(reimbursementAuthorEle);
         cardEle.appendChild(reimbursementResolverEle);
         cardEle.appendChild(reimbursementStatusID_FKEle);
         cardEle.appendChild(reimbursementTypeID_FKEle);
      
         managerCardEle.appendChild(cardEle)
                } 
     })
        }
    }

}

//end session and redirect to login when logout button is clicked
let logoutBtn = document.getElementById("logout-btn")
logoutBtn.onclick = async function(){
    let logoutRes = await fetch(`${domain}/api/logout`)

    let logoutResData = await logoutRes.json();

    if(logoutResData.success)
        window.location = `${domain}/`
}

