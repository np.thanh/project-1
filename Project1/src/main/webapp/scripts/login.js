let loginBtn = document.getElementById("submit");

window.onload = async function(){
     const sessionRes = await fetch(`${domain}/api/check-session`)
    const sessionDataRes = await sessionRes.json()

if(sessionDataRes.data){
    if(sessionDataRes.data.userRoleID_FK == 1){
        window.location = `${domain}/employee?id=${sessionDataRes.data.userID}`
    } else if(sessionDataRes.data.userRoleID_FK == 2){
        window.location = `${domain}/manager?id=${sessionDataRes.data.userID}`
    }
}
} 

loginBtn.onclick = async function(e){
    e.preventDefault();

    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;

    let response = await fetch(`${domain}/api/login`, {
        method: "POST",
        body: JSON.stringify({
            username: username,
            password: password
        })
    })

    document.getElementById("username").value = ''
    document.getElementById("password").value = ''

    let responseData = await response.json();
    console.log(responseData);

    //console.log(responseData.data.userRoleID_FK);
    if(responseData.success){
        if(responseData.data.userRoleID_FK == 1){
            window.location = `${domain}/employee?id=${responseData.data.userID}`
        } else if(responseData.data.userRoleID_FK == 2){
            window.location = `${domain}/manager?id=${responseData.data.userID}`
        }
    } else{
        let messageElem = document.getElementById("login-message")
        messageElem.style = "background-color: white;"
        messageElem.innerText = responseData.message
    }


}

