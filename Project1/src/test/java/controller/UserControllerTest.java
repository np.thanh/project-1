package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
class UserControllerTest {
    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    UserController userController;
    @Mock
    UserService userService;

    @BeforeEach
    void setUp(){
        userController = UserController.getInstance();
        userService = new UserService();
    }



    @Test
    void login() throws IOException, ServletException {


        User user = new User(
                1,
                "test1",
                "pass1",
                "fname1",
                "lname1",
                "test@yahoo.com",
                1);


        Stream stream = Mockito.mock(Stream.class);
        when(stream.collect(Collectors.joining(System.lineSeparator()))).thenReturn(user );

        BufferedReader reader = Mockito.mock(BufferedReader.class);
        when(reader.lines()).thenReturn(stream);

        when(request.getReader()).thenReturn(reader);

        userController.login(request, response);
        Mockito.verify(userController, Mockito.times(1)).login(request, response);

        //servlet.service(request, response);

      /*  verify(servlet, times(1)).service(request, response);

        when(request)


       StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);

        when(response.getWriter()).thenReturn(writer);
*/


    }

    @Test
    void register() {
    }

    @Test
    void getAllUser() {
    }

    @Test
    void checkSession() {
    }

    @Test
    void logout() {
    }
}