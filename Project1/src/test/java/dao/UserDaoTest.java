package dao;

import model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import service.UserService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UserDaoTest {
    UserDao userDao = Mockito.mock(UserDao.class);
    User user;

    @BeforeEach
    void setUp(){
        user = new User();
    }

    @Test
    public void addUser() {
        //Assign
        User user = new User(
                1,
                "test1",
                "pass1",
                "fname1",
                "lname1",
                "test@yahoo.com",
                1);
        Mockito.when(userDao.addUser(user)).thenReturn(user);

        User expectedResult = user;

        //Act
        User actualResult = userDao.addUser(user);

        //Assert
        assertEquals(expectedResult, actualResult);
        Mockito.verify(userDao, Mockito.times(1)).addUser(user);

    }

    @Test
    void getUser() {
        //Assign
        User user = new User(
                1,
                "test1",
                "pass1",
                "fname1",
                "lname1",
                "test@yahoo.com",
                1);
        Mockito.when(userDao.getUser(user.getUsername())).thenReturn(user);

        User expectedResult = user;

        //Act
        User actualResult = userDao.getUser(user.getUsername());

        //Assert
        Mockito.verify(userDao, Mockito.times(1)).getUser(user.getUsername());

    }

    @Test
    void getAllUsers() {
        //Assign
        User user = new User(
                1,
                "test1",
                "pass1",
                "fname1",
                "lname1",
                "test@yahoo.com",
                1);

        User expectedResult = user;

        //Act
        List<User> actualResult = userDao.getAllUsers();


        Mockito.verify(userDao, Mockito.times(1)).getAllUsers();
    }
}