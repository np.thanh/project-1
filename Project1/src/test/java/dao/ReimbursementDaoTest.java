package dao;

import model.Reimbursement;
import model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import service.EmployeeService;
import service.ManagerService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ReimbursementDaoTest {
    User user;
    ReimbursementDao reimbursementDao = Mockito.mock(ReimbursementDao.class);

    @BeforeEach
    void setUp(){
        user = new User();
    }

    @Test
    void employeeViewAllTickets() {
        //Assign
        Reimbursement reimbursement = new Reimbursement(
                1,
                45.5,
                null,
                null,
                "Eat out",
                null,
                1,
                2,
                2,
                1
        );


        //Act
        List<Reimbursement> actualResult = reimbursementDao.employeeViewAllTickets(1);

        Mockito.verify(reimbursementDao,Mockito.times(1)).employeeViewAllTickets(1);
    }

    @Test
    void addReimbursement() {
        //Assign
        Reimbursement reimbursement = new Reimbursement(
                1,
                45.5,
                null,
                null,
                "Eat out",
                null,
                1,
                2,
                2,
                1
        );

        //Expected Result
        reimbursementDao.addReimbursement(reimbursement);

        Mockito.verify(reimbursementDao,Mockito.times(1)).addReimbursement(reimbursement);
    }

    @Test
    void viewAllReimbursement() {
        //Act
        reimbursementDao.viewAllReimbursement();
        Mockito.verify(reimbursementDao,Mockito.times(1)).viewAllReimbursement();
    }

    @Test
    void updateStatus() {
        //Assign
        Reimbursement reimbursement = new Reimbursement(
                1,
                45.5,
                null,
                null,
                "Eat out",
                null,
                1,
                2,
                2,
                1
        );

        reimbursementDao.updateStatus(1, 1, 1);
        Mockito.verify(reimbursementDao,Mockito.times(1)).updateStatus(1, 1, 1);

    }

    @Test
    void viewAllReimbursementByStatus() {

        reimbursementDao.viewAllReimbursementByStatus(1);
        Mockito.verify(reimbursementDao, Mockito.times(1)).viewAllReimbursementByStatus(1);
    }
}