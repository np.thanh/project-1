package frontcontroller;

import model.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DispatcherTest{
    private final static String URI = "/Project1/api/login";
    @Test
    void service() throws ServletException, IOException {
        final Dispatcher servlet = mock(Dispatcher.class);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        //final RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        User user = new User(
                1,
                "test1",
                "pass1",
                "fname1",
                "lname1",
                "test@yahoo.com",
                1);

        when(request.getRequestURI()).thenReturn(URI);
        when(request.getMethod()).thenReturn("POST");

        Stream stream = Mockito.mock(Stream.class);
        when(stream.collect(Collectors.joining(System.lineSeparator()))).thenReturn(user.toString());

        BufferedReader reader = Mockito.mock(BufferedReader.class);
        when(reader.lines()).thenReturn(stream);

        when(request.getReader()).thenReturn(reader);



        //servlet.service(request, response);

        verify(servlet, times(1)).service(request, response);

        //verify(request, times(1)).getRequestDispatcher(URI);
        //verify(request, never()).getSession();
        //verify(URI).forward(request, response);
    }
}