package service;

import model.Reimbursement;
import model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ManagerServiceTest {
    User user;
    ManagerService managerService = Mockito.mock(ManagerService.class);

    @BeforeEach
    void setUp(){
        user = new User();

    }

    @Test
    void managerViewAllReimbursement() {
        List<Reimbursement> actualResult = managerService.managerViewAllReimbursement();

        Mockito.verify(managerService, Mockito.times(1)).managerViewAllReimbursement();
    }

    @Test
    void managerUpdateStatus() {
        managerService.managerUpdateStatus(1, 1, 1);
        Mockito.verify(managerService,Mockito.times(1)).managerUpdateStatus(1, 1, 1);
    }

    @Test
    void managerViewAllRequestByStatus() {
        managerService.managerViewAllRequestByStatus(1);
        Mockito.verify(managerService, Mockito.times(1)).managerViewAllRequestByStatus(1);
    }
}