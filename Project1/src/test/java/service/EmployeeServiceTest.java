package service;

import model.Reimbursement;
import model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeServiceTest {
    User user;
    EmployeeService employeeService = Mockito.mock(EmployeeService.class);

    @BeforeEach
    void setUp(){
        user = new User();

    }

    @Test
    void employeeViewReimbursement() {
        List<Reimbursement> actualResult = employeeService.employeeViewReimbursement(1);

        Mockito.verify(employeeService,Mockito.times(1)).employeeViewReimbursement(1);
    }

    @Test
    void addReimbursementRequest() {
        //Assign
        Reimbursement reimbursement = new Reimbursement(
                1,
                45.5,
                null,
                null,
                "Eat out",
                null,
                1,
                2,
                2,
                1
        );
        employeeService.addReimbursementRequest(reimbursement);

        Mockito.verify(employeeService,Mockito.times(1)).addReimbursementRequest(reimbursement);

    }
}