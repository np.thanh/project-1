package service;

import dao.UserDao;
import model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {
    UserService userService = Mockito.mock(UserService.class);
    UserDao userDao = Mockito.mock(UserDao.class);
    User user;

    @BeforeEach
    void setUp(){
        user = new User();
        }

    @Test
    void addUser() {
        User user = new User(
                1,
                "test1",
                "pass1",
                "fname1",
                "lname1",
                "test@yahoo.com",
                1);

        User expectedResult = user;

        Mockito.when(userService.addUser(user)).thenReturn(user);

        User actualResult = userService.addUser(user);

        assertEquals(expectedResult, actualResult);

    }

    @Test
    void getUser() {
        User user = new User(
                1,
                "test1",
                "pass1",
                "fname1",
                "lname1",
                "test@yahoo.com",
                1);

        User expectedResult = user;

        Mockito.when(userService.getUser(user)).thenReturn(user);

        User actualResult = userService.getUser(user);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    void getAllUser() {
        List<User> actualResult = userService.getAllUser();

        Mockito.verify(userService, Mockito.times(1)).getAllUser();

    }
}