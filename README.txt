----- PROJECT NAME ----
EXPENSE REIMBURSEMENT APP

---- PROJECT DESCRIPTION ----
The Expense Reimbursement App will manage the process of reimbursing employees for expenses incurred while on company time. 
All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. 
Finance managers can log in and view all reimbursement requests and past history for all employees in the company. 
Finance managers are authorized to approve and deny requests for expense reimbursement.

---- TECHNOLOGY USED ----
- Java
- JavaScript
- HTML
- CSS
- PostGreSQL
- AWS RDS
- AWS S3

---- FEATURES ----
- Users can login using username and password provided.
- After login, the website will auto-direct to the employee dashboard or manager dashboard based on the users's infomation.
- As a employee, he/she will be able to view all of his/her reimbursements, all pending reimbursements, and submit a new reimbursement.
- As a manager, he/she will be able to view all reimbursements, view by status, approve or deny a reimbursement.
- Check session for each user.

---- GETTING STARTED ----
- Open the project in the InteliJ, set up TomCat
- Run TomCat
- Open http://localhost:9000/Project1/api/login on the brower
- Enjoy the program